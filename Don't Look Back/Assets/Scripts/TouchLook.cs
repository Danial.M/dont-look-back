using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchLook : MonoBehaviour
{
    public float joystickSensitivity = 100f;

    public Joystick lookJoystick;

    float xRotation = 0f;
    float yRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        // Deaktiviere die Cursor-Sperre
        Cursor.lockState = CursorLockMode.None;
    }

    // Update is called once per frame
    void Update()
    {
        // Überprüfe, ob der Joystick aktiv ist
        if (lookJoystick.isActiveAndEnabled)
        {
            // Erfasse die Joystick-Eingabewerte
            float joystickX = lookJoystick.Horizontal * joystickSensitivity * Time.deltaTime;
            float joystickY = lookJoystick.Vertical * joystickSensitivity * Time.deltaTime;

            // Führe die Rotation basierend auf den Joystick-Eingabewerten durch
            xRotation -= joystickY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            yRotation += joystickX;
            yRotation %= 360f;

            transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
        }
    }
}

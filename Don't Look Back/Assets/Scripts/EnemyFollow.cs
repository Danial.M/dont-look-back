using UnityEngine;
using UnityEngine.AI;

public class EnemyFollow : MonoBehaviour
{
    public GameObject Player;

    void Update()
    {
        gameObject.GetComponent<NavMeshAgent>().SetDestination(Player.transform.position);
    }
}

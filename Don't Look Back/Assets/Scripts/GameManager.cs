using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    // bool gameHasEnded = false;

    //public GameObject startMazeUI;
    public GameObject GameOverUI;
    public GameObject ControllerUI;
    public GameObject Elmo;


    public void StartGame()
    {
        // Game has ended set to true, so EndGame() function cannot be called
        //startMazeUI.SetActive(true);
        Debug.Log("Game STARTED");
    }

    public void GameOver()
    {
        Elmo.SetActive(false);
        ControllerUI.SetActive(false);
        GameOverUI.SetActive(true);
        Debug.Log("GAME OVER");
    }

}

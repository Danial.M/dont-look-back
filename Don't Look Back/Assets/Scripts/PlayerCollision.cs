using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement movement;     // Reference to PlayerMovement script

    private void OnTriggerEnter(Collider hit)
    {
        // Check if player collided with tag "Enemy"
        if (hit.tag == "EndPoint")
        {
            Debug.Log("We hit the start");
            movement.enabled = false; // Disable players movement
            FindObjectOfType<GameManager>().StartGame();
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        if (hit.tag == "Enemy")
        {
            Debug.Log("We hit an enemy");
            movement.enabled = false; // Disable players movement
            FindObjectOfType<GameManager>().GameOver();
        }
    }
    
}

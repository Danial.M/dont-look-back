using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void StartGame()
    {
        Debug.Log("Start Game");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void RestartGame()
    {
        Debug.Log("Restart Game");
        SceneManager.LoadScene(0);
    }

    // public void QuitGame()
    // {
    //     Debug.Log("End Game");
    //     Application.Quit();
    // }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public Joystick movementJoystick;
    public Camera mainCamera; // Referenz auf die Hauptkamera

    public float speed = 12f;
    public float gravity = -9.81f;
    // public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;

    public Button jumpButton; // Referenz auf den Jump-Button

    // Start is called before the first frame update
    void Start()
    {
        // Füge den Event-Handler für den Jump-Button hinzu
        // jumpButton.onClick.AddListener(Jump);
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = movementJoystick.Horizontal;
        float z = movementJoystick.Vertical;

        // Berechnung der Bewegungsrichtung abhängig von der Kameraausrichtung
        Vector3 cameraForward = mainCamera.transform.forward;
        Vector3 cameraRight = mainCamera.transform.right;
        cameraForward.y = 0f;
        cameraRight.y = 0f;
        cameraForward.Normalize();
        cameraRight.Normalize();

        Vector3 move = cameraRight * x + cameraForward * z;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    // void Jump()
    // {
    //     if (isGrounded)
    //     {
    //         velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
    //     }
    // }
}

Shader "Custom/TextureStretchShader" {
    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _TileDensity ("Tile Density", Range(1, 10)) = 1
    }
 
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _MainTex;
        float _TileDensity;

        struct Input {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o) {
            fixed2 uv = IN.uv_MainTex * _TileDensity;
            o.Albedo = tex2D(_MainTex, uv);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
